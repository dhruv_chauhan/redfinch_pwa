# A progressive Web application with Vue JS

![](https://lh3.googleusercontent.com/OX-ZIPKIZbZt_P8uQIWvj7ZAUqtZd3ms86iKUH4utkH6BYFEemy5Vgq9HZY2nTBak5M281JMF9-g17xHD2lVKRbeS-0SGZxZUW-ffEbTj6IK8nuiRGrs_T1F2tu0uS3GiPvBpKYI "horizontal line")

# “A Progressive Web App uses modern web capabilities to deliver an app-like user experience.”

[ Readings : [Progressive Web Apps](https://developers.google.com/web/progressive-web-apps/?hl=en) , [What is a Progressive Web App](https://blog.ionicframework.com/what-is-a-progressive-web-app/) , [PWA:Bridging the gap between web and mobile apps](https://medium.freecodecamp.org/progressive-web-apps-bridging-the-gap-between-web-and-mobile-apps-a08c76e3e768) , [Why Progressive Web Apps Will Replace Native Mobile Apps](https://www.forbes.com/sites/forbestechcouncil/2018/03/09/why-progressive-web-apps-will-replace-native-mobile-apps/#4cff296c2112) , [The Ultimate Guide to Progressive Web Applications](https://scotch.io/tutorials/the-ultimate-guide-to-progressive-web-applications) ]

# Introduction

Progressive Web Apps are experiences that combine the best of the web and the best of apps. They are useful to users from the very first visit in a browser tab, no install required. As the user progressively builds a relationship with the app over time, it becomes more and more powerful. It loads quickly, even on flaky networks, sends relevant push notifications, has an icon on the home screen, and loads as a top-level, full screen experience.

# What is a Progressive Web App?

[ Readings : [Progressive Web App Checklist](https://developers.google.com/web/progressive-web-apps/checklist) , [service worker](https://developers.google.com/web/fundamentals/getting-started/primers/service-workers) , [W3C manifest](https://developers.google.com/web/updates/2014/11/Support-for-installable-web-apps-with-webapp-manifest-in-chrome-38-for-Android) , [service worker registration](https://developers.google.com/web/fundamentals/instant-and-offline/service-worker/registration)]

A Progressive Web App is:

-   Progressive - Works for every user, regardless of browser choice because it's built with progressive enhancement as a core tenet.
    
-   Responsive - Fits any form factor: desktop, mobile, tablet, or whatever is next.
    
-   Connectivity independent - Enhanced with service workers to work offline or on low-quality networks.
    
-   App-like - Feels like an app, because the app shell model separates the application functionality from application content .
    
-   Fresh - Always up-to-date thanks to the service worker update process.
    
-   Safe - Served via HTTPS to prevent snooping and to ensure content hasn't been tampered with.
    
-   Discoverable - Is identifiable as an "application" thanks to W3C manifest and service worker registration scope, allowing search engines to find it.
    
-   Re-engageable - Makes re-engagement easy through features like push notifications.
    
-   Installable - Allows users to add apps they find most useful to their home screen without the hassle of an app store.
    
-   Linkable - Easily share the application via URL, does not require complex installation.
    

A Progressive Web Application (PWA) is a web application that offers an app-like user experience. PWAs benefit from the modern Web technological innovations (Service Workers, Native APIs, JS frameworks) and raise web application quality standard. Progressive Web Apps bring features we expect from native apps to the mobile browser experience in a way that uses standards-based technologies and run in a secure container accessible to anyone on the web.

From the developer’s point of view, PWAs are basically a website, so:

-   you can write them with any framework you want;
    
-   one code to rule them all: it is cross-platform and cross-devices (the code is executed by user’s browser);
    
-   easy to ship: no need to download it through a Store.
    

  
  
  
  
  
  

### This documentation aims to create a basic but complete progressive web application where the application architecture consists of a front-end comprised of a Vue.js Single Page Application (SPA) / Webpack and a backend REST API using the Flask web framework, from scratch.

  

# Creating Single Page Application with VueJS, Webpack and Material Design Lite

Checklist we’ll be following for our PWA,

![](https://lh4.googleusercontent.com/GiAoJdaHzNGBudKWKzxLpvWYrl5UhGOR6LK3yfWgpFrYqApSnhn-hHbXrOI32Y3ABqZAX4-EC46IAVRqWheYL1EiXEASJgA2yqqYHjEmFIgkfkzX6XCwE5dodwj9Svhh0xJCKRCz)

  

1.  ### Build the VueJS App base
    

[ Redings : [Vue.js guide](https://vuejs.org/v2/guide/index.html) , [vue-cli](https://github.com/vuejs/vue-cli) , [vuejs-templates](https://github.com/vuejs-templates) , [vuejs-templates/pwa](https://github.com/vuejs-templates/pwa) ]

We are going to use Vue-cli to scaffold our application. Vue-cli comes along with a few templates. We will choose pwa template. Vue-cli is going to create a dummy VueJS application with Webpack, vue-loader (hot reload), a proper manifest file and basic offline support through service workers.
```
npm install -g vue-cli
vue init pwa redfinch_pwa
```
  
```
? Project name pwa

? Project short name: fewer than 12 characters to not be truncated on homescreens (default: same as name) redfinch

? Project description A VueJS Project

? Author Dhruv <dhruv.chauhan98@gmail.com>

? Vue build standalone

? Install vue-router? Yes

? Use ESLint to lint your code? No

? Setup unit tests with Karma + Mocha? No

? Setup e2e tests with Nightwatch? No

vue-cli · Generated "redfinch_pwa".
```
  

This process creates a project folder with following subfolders:

-   build: contains webpack and vue-loader configuration files
    
-   config: contains our app config (environments, parameters…)
    
-   src: source code of our application
    
-   static: images, css and other public assets
    

  

To start the dev server,
```
cd redfinch_pwa/frontend

npm install

npm run dev
```
  

### 2) make your application installable

[Readings : complete list of manifest options on [Mozilla Developer website](https://developer.mozilla.org/en-US/docs/Web/Manifest) , [ngrok](https://ngrok.com/)]

Web applications that provides a valid manifest.json file in their index.html are installable. Vue pwa template provides a default manifest.json file, which you can customize for your project.
```
{
"name": "pwa",
"short_name": "redfinch",
"icons": [
{
"src": "/static/img/icons/android-chrome-192x192.png",
"sizes": "192x192",
"type": "image/png"
},
{
"src": "/static/img/icons/android-chrome-512x512.png",
"sizes": "512x512",
"type": "image/png"
}
],
"start_url": "/",
"display": "fullscreen",
"orientation": "portrait",
"background_color": "#2196f3",
"theme_color": "#2196f3"
}
```
Make sure manifest is linked to the index.html and you also have a viewport declared in the head section.
```
…
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="<%= htmlWebpackPlugin.files.publicPath %>static/manifest.json">
...
```

To try to install redfinch_pwa on a mobile device, we’d have to find a way to access our localhost:8080 from a distant mobile device. One such way is using ngrok.

To install and run ngrok:
```
npm install -g ngrok
ngrok http 8080
```

### In the output, you’ll have see a Forwarding address, browse this URL with your smartphone and you’ll see that you can add the app to your device desktop too.

### 3) Install Material Design Lite

[ Readings : [Material Design](https://material.io/guidelines/) ]

It’s a great framework that let you implement Material Design easily and lightly on your web application.
```
npm install material-design-lite --save
```
  

### 4) Create view skeleton and handle routing

[ Readings : [Vue components life cycle](https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram) , [vue-components](https://vuejs.org/v2/guide/components.html) , [vue-router](https://router.vuejs.org/) , [v-for](https://vuejs.org/v2/guide/list.html) , [v-bind](https://vuejs.org/v2/guide/class-and-style.html) , [v-model](https://vuejs.org/v2/guide/forms.html) , [v-on](https://vuejs.org/v2/guide/events.html)]

index.html is the only HTML file that a Vue SPA utilizes and it contains the lone div element that is produced with a default id of "app" is what the main Vue instance will attach to. That Vue object injects the HTML and CSS that are in the components into the div to produce the UI.
```
<html>
<head>
...
</head>
<body>
<noscript>
This is your fallback content in case JavaScript fails to load.
</noscript>
<div id="app"></div>
<!-- Todo: only include in production -->
<%= htmlWebpackPlugin.options.serviceWorkerLoader %>
<!-- built files will be auto injected -->
</body>
</html>
```
main.js file is the primary entry point for the application and is where you will register the Vue instance and extensions such as vue-router and vuex. As you can see this is where the Vue instance resides. The instance is registered to the app div discussed previously, plus it is fed the router object and the high-level App component.

To activate the store (to be initialized later) import the store module as a property.

```
import  Vue  from  'vue'
import  App  from  './App'
import  router  from  './router'
import  store  from  './store'
  
Vue.config.productionTip  =  false
/* eslint-disable no-new */
new  Vue({
el: '#app',
router,
store,
template: '<App/>',
components: { App }
})
```

The App.vue file serves as the top level application components and often contains the general layout of the application.
```
<template>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
<header  class="mdl-layout__header">
<div class="mdl-layout__header-row">
<span class="mdl-layout-title">Redfinch</span>
</div>
</header>
<div class="mdl-layout__drawer">
<span class="mdl-layout-title">Redfinch</span>
<nav  class="mdl-navigation">
<router-link  class="mdl-navigation__link"  to="/"  @click.native="hideMenu">Home</router-link>
<router-link  class="mdl-navigation__link"  to="/catalog"  @click.native="hideMenu">Catalog</router-link>
</nav>
</div>
<main  class="mdl-layout__content">
<div class="page-content">
<router-view></router-view>
</div>
</main>
</div>
</template>
<script>
require('material-design-lite')
export  default {
name: 'app',
methods: {
hideMenu: function () {
document.getElementsByClassName('mdl-layout__drawer')[0].classList.remove('is-visible')
document.getElementsByClassName('mdl-layout__obfuscator')[0].classList.remove('is-visible')
}
}
}
</script>
<style>
@import  url('https://fonts.googleapis.com/icon?family=Material+Icons');
@import  url('https://code.getmdl.io/1.2.1/material.blue-red.min.css');
</style>
```
The index.js script in the router directory is where the URLs for the application are defined and mapped to components. To map a route path to a component it first has to be imported, then you need to define a route object in the routes array giving it a path, a name, and the component to be displayed.

> the :id portion of the new path /products/:id. This is known as a dynamic segment which you can think of as a variable within a route path.

```
import  Vue  from  'vue'
import  BootstrapVue  from  'bootstrap-vue'
import  Router  from  'vue-router'
import  store  from  '@/store'
import  Home  from  '@/components/Home'
import  Catalog  from  '@/components/Catalog'
import  Products  from  '@/components/Products'
import  'bootstrap/dist/css/bootstrap.css'
import  'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(Router)

export  default  new  Router({
mode: 'history',
routes: [
{
path: '/',
name: 'Catalog',
component: Catalog
},{
path: '/catalog',
name: 'Catalog',
component: Catalog
},{
path: '/catalog/:id',
name: 'Products',
component: Products
},
]
})
```
The "components" directory is where UI components reside. Take a look at the contents of the script section. Here you will see that an object is being exported. This object will get injected into the Vue instance that was initialized in the main.js file. Inside this JavaScript object is a data method which returns an object. This object is where you can place component-level state (data).

> In the template section of each vue component, a div has to be added which will wrap all my other elements. Every Vue component must have a single parent element, there cannot be top-level sibling elements or the component will not compile.

> A Vue component object's methods section is where functions can be defined to do a number of things, most often to change component state.

```
<template>
<nav  class="navbar is-light"  role="navigation"  aria-label="main navigation">
<div class="navbar-menu">
<div class="navbar-start">
<router-link  to="/catalog"  class="navbar-item">
Catalog
</router-link>
</div>
</div>
</nav>
</template>

<script>
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style  scoped>
</style>
```

> < router-link> is a component that receives a route path via the to attribute, which is actually called a "parameter" in a Vue component. The < router-link> component produces a hyperlink element that responds to click events and tells Vue to display the component associated with its to parameter.

```
<template>
<div class="mdl-grid">
<div class="mdl-cell mdl-cell--3-col mdl-cell mdl-cell--1-col-tablet mdl-cell--hide-phone"></div>
<div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-phone">
<div v-for="product  in  products" v-bind:key="product.id">
<div class="image-card__picture">
<p><a v-bind:href="'/catalog/'+product.id">
<img v-bind:src="product.img_src" v-bind:alt="product.name"/>
</a></p>
</div>
<div class="image-card__comment mdl-card__actions">
<p><a v-bind:href="'/catalog/'+product.id">
<span>{{ product.name }}</span>
</a></p>
</div>
</div>
</div>
</div>
</template>

<script>
import { mapState } from  "vuex";
export  default {
computed: mapState({
products: state  =>  state.products
}),
beforeMount() {
this.$store.dispatch("loadProducts");
}
};
</script>

<style  scoped>
.add-picture-button {
position: fixed;
right: 24px;
bottom: 24px;
z-index: 998;
}
.take-picture-button {
position: fixed;
right: 24px;
bottom: 90px;
z-index: 5;
}
.image-card {
position: relative;
margin-bottom: 8px;
}
.image-card__picture  >  img {
width:100%;
}
.image-card__comment {
position: relative;
bottom: 0;
height: 52px;
padding: 10px;
text-align: right;
background: rgba(0, 0, 0, 0.3);
}
.image-card__comment  >  span {
color: #fff;
font-size: 20px;
font-weight: bold;
}
</style>
```

> The v-for directive uses a syntax of item in items where items is an iterable such as an array or an object's properties and item is an alias for each element in the iterable. The v-bind:key part is adding an attribute called key equal to the product's id to each div. Vue uses these keys to keep explicit track of each node being created in the DOM which aids in bookkeeping and performance.

> Vue components have a series of defined lifecycle stages that are meaningful to the developer when doing various things such as pulling in data using an AJAX request. In order to feed product data into the Home component I will need to hook into one of the Vue lifecycle stages, specifically the beforeMount stage.
> 
> The beforeMount lifecycle stage works well for the API call because it is executed right before the mounting of our component begins, and right before render is called on our component. This gives it time to fetch data before being displayed to the user.

```
<template>
<div class="mdl-grid" align="center">
<div class="mdl-cell mdl-cell--8-col">
<h2>{{ product.name }}</h2>
<div class="picture">
<img v-bind:src="product.img_src" v-bind:alt="product.name"/>
</div>
<div class="info">
<h3>Product Description</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
<p>Category : {{ product.category }}</p>
<p>Language : {{ product.language }}</p>
<p>Price : {{ product.price }}</p>
</div>
</div>
<div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet">
<div class="comment" align="center">
<textarea type="text" v-model="text" placeholder="Comment"></textarea>
<input type="text" v-model="author" placeholder="Author name">
<button v-on:click="appendComment">Add Comment</button>
</div>
<div class="comments-box" v-for="comment  in  product.comments.reverse()" v-bind:key="comment.id" align="left">
<p class="author">{{ comment.author }} :</p>
<p class="text"> {{ comment.text }}</p>
</div>
</div>
<router-link  class="whishlist-picture-button mdl-button mdl-js-button mdl-button--fab mdl-button--colored"  to="/">
<i class="material-icons">Whishlist it!</i>
</router-link>
<router-link  class="cart-picture-button mdl-button mdl-js-button mdl-button--fab mdl-button--colored"  to="/">
<i class="material-icons">Cart it!</i>
</router-link>
</div>
</template>

<script>
export  default {
methods: {
data() {
return {
author: '',
text: '',
}
},
appendComment (){
const  comment  = { author: this.author, text: this.text }
this.product.comments.push(comment);
this.addComment(this.product.id, comment);
this.author="";
this.text="";
},
addComment(pid, comment){
this.$store.dispatch('addComment', { pid, comment });
},
},
beforeMount() {
this.$store.dispatch('loadProduct', { id: parseInt(this.$route.params.id) })
},
computed: {
product() {
return  this.$store.state.currentProduct
}
},
}
</script>

<style  scope>
*{
padding: 0;
margin: 0;
font-family: Arial, Helvetica, sans-serif;
font-size: 16px;
}
#demo{
margin: 20px  0  0  0;
}
textarea{
width: 100%;
border: 2px  solid  #ccc;
border-radius: 7px;
height: 70px;
font-family: Verdana, Helvetica, sans-serif;
padding: 5px;
}
input{
border: 2px  solid  #ccc;
border-radius: 5px;
padding: 5px;
}
button{
background: #333;
color: #ccc;
border: 0;
padding: 5px;
cursor: pointer;
}
/*Comment Box*/
.comments-box{
width: 90%;
margin: auto;
padding: 20px  0;
border-bottom: 1px  solid  #000;
}
.author{
color: rgb(221, 63, 63);
margin: 10px  0;
font-weight: bold;
}
.text{
color: rgb(63, 10, 10);
margin: 10px  0;
font-weight: italic;
}
.picture  >  img {
color: #fff;
width:100%;
}
.info {
text-align: right;
padding: 5px;
color: #555;
font-size: 10px;
}
.comment {
padding: 10px;
color: #555;
}
.actions {
text-align: center;
}
.whishlist-picture-button {
position: fixed;
right: 24px;
bottom: 24px;
z-index: 998;
}
.cart-picture-button {
position: fixed;
right: 24px;
bottom: 90px;
z-index: 5;
}
</style>
```

>Use the v-model directive to create two-way data bindings on form input and textarea elements. It automatically picks the correct way to update the element based on the input type. The text inputs have v-model registered to them with the data properties text and author attached to them, which keeps the text input in sync with the text and author data properties of the Vue instance.

>The "Add Comment" button element has v-on:click=”appendComment" within it, which is a Vue click event handler that responds by calling the appendComment function inside the methods Vue object property.”

  

### 5) State Management with Vuex

[ Read : [Vuex](https://vuex.vuejs.org/en/intro.html) , [Flux](https://code-cartoons.com/a-cartoon-guide-to-flux-6157355ab207) ]

Vuex is a centralized state management library officially supported by the core Vue.js development team. Vuex provides a flux-like, unidirectional data flow, pattern that is proven to be very powerful in supporting moderate to large Vue.js applications.

Install vuex
```
$ npm install --save vuex
```
Next I add a new directory within the project's src/ directory called "store" and add an index.js file. Inside the store/index.js file I begin by adding the necessary imports for Vue and Vuex objects then attach Vuex to Vue using Vue.use(Vuex) similar to what was done with vue-router. After this I define four stubbed out JavaScript objects: state, actions, mutations, and getters.
```
import  Vue  from  'vue'
import  Vuex  from  'vuex'
import {
fetchProducts,
fetchProduct,
postComment
} from  '@/api'
Vue.use(Vuex)
```

 >The state object will serve as the single source of truth where all the important application-level data is contained within the store. This state object will contain product data that can be accessed and watched for changes by any components interested in them such as the Home component.
```
const  state  = {
products: [],
currentProduct: {},
}
```
I initialize an empty products array in the state object within store/index.js. This will be the location where all application level product data will reside once pulled in by an AJAX request.

Now that the products have a place to reside I need to create an action method, loadProducts(...), that can be dispatched from the Home component (or any other component requiring product data) to handle the asynchronous request to the mock AJAX function fetchProducts(). To use fetchProducts() I first need to import it from the api module then define the loadProducts(...) action method to handle making the request.

>The actions object is where I will define what are known as action methods. Action methods are referred to as being "dispatched" and they're used to handle asynchronous operations such as AJAX calls to an external service or API.
```
const  actions  = {
// asynchronous operations
loadProducts(context) {
return  fetchProducts()
.then((response) => {
context.commit('setProducts', {
products: response.data
})
})
},
loadProduct(context, {
id
}) {
return  fetchProduct(id)
.then((response) => {
context.commit('setProduct', {
product: response.data
})
})
},
addComment(context, data) {
return  postComment(data)
}
}
```
Actions often work in tandem with mutations in a pattern of performing asynchronous AJAX requests for data to a server followed by explicitly updating the store's state object with the fetched data. Once the mutation is committed then the parts of the application using the products will recognize there are updated products via Vue's reactivity system. Here the mutation I am defining is called setProducts(...).

>The mutations object provides methods which are referred to being "committed" and serve as the one and only way to change the state of the data in the state object. When a mutation is committed any components that are referencing the now reactive data in the state object are updated with the new values, causing the UI to update and re-render its elements.
```
const  mutations  = {
setProducts(state, payload) {
state.products  =  payload.products
},
setProduct(state, payload) {
state.currentProduct  =  payload.product
},
}
```
The getters object contains methods also, but in this case they serve to access the state data utilizing some logic to return information. Getters are useful for reducing code duplication and promote reusability across many components.

  

The store now possesses the ability to fetch products I can update the Catalog component and utilize the store to feed it product data using an import to the vuex helper function called mapState to map the products array that resides in the state object to a computed property also called products.
```
import { mapState } from 'vuex'
```
Additionally, in the beforeMount method I trigger the dispatch of the loadProducts store action. I am able to access the store and dispatch the action method with the syntax this.$store.dispatch(...).

  

Next I migrated the Product component to utilize vuex's store to fetch the specific product to participate in taking using another store action method which can then save (ie, commit a mutation) the fetched product to a state property I will name currentProduct (instead of accessing the :id prop of the route).

I use fetchProduct in a new action method named loadProduct which then commits a mutation in another new method within the mutations object called setCurrentProduct.

Once a new comment is submitted, I will engage vuex's store and associated pattern of dispatching an action to POST the new comment to the server. The Product component can then fetch all comments including the new one.

At the end of the file I define a final object, which is an instance of the Vuex.Store({}) object, that pulls all the other stub objects together, and then it is exported.
```
const  store  =  new  Vuex.Store({
state,
actions,
mutations,
getters
})
export  default  store
```

### 6) RESTful API with Flask

[ Readings : [Flask](http://flask.pocoo.org/) , [RESTful API](https://searchmicroservices.techtarget.com/definition/RESTful-API) , [virtual environment](http://stackabuse.com/python-virtual-environments-explained/) , [PostgreSQL](https://www.postgresql.org/docs/) , [ElephantSQL](https://www.elephantsql.com/docs/)]

I begin in the /backend directory by creating a Python3 virtual environment and installing Flask and a few other necessary libraries.
```
$ python -m venv venv  
$ source venv/bin/activate  
(venv) $ pip install Flask Flask-SQLAlchemy Flask-Migrate Flask-Script requests
```
In the /backend directory I make a few new files called manage.py and appserver.py. Also, I will make a new directory inside of /backend that will become my "frontendapi" Flask application. Within the frontendapi directory I make the files __init__.py, models.py, application.py, and api.py.

```
import os
basedir = os.path.abspath(os.path.dirname(__file__))

class  Config(object):
DEBUG  =  True
TESTING  =  False
CSRF_ENABLED  =  True
SQLALCHEMY_TRACK_MODIFICATIONS  =  False
SECRET_KEY  =  'my-secret-key'
SQLALCHEMY_DATABASE_URI  =  'postgres://znjpepoy:2SjD3Kh5BcvJhvtydWjrDremWPAxRZVh@stampy.db.elephantsql.com:5432/znjpepoy'
```

This config class defines a SQLALCHEMY_DATABASE_URI application database connection URI to a single file SQLite database.

```
from flask import Flask
from flask_cors import  CORS
def  create_app(app_name='FRONTEND_API'):

app = Flask(__name__)
app.config.from_object('frontendapi.config.Config')

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

from frontendapi.api import api
app.register_blueprint(api, url_prefix="/api")

from frontendapi.models import db
db.init_app(app)
return app
```

Inside of application.py I will create a Flask application instance. In addition to instantiating an instance of Flask it also sources the BaseConfig object and registers the API routes blueprint and register the database object, db, with the Flask application object.

```
from flask import Blueprint, jsonify, request
from .models import db, Product, Comment
api = Blueprint('api', __name__)  
@api.route('/catalog/', methods=('GET', 'POST'))

def  fetchProducts():
products = Product.query.all()
return jsonify([p.to_dict() for p in products])

@api.route('/catalog/<int:id>/', methods=('GET', 'POST'))
def  fetchProduct(id):
if request.method ==  'GET':
product = Product.query.get(id)
return jsonify(product.to_dict())
elif request.method ==  'POST':
data = request.get_json()
comment = Comment(author=data['author'], text=data['text'])
product = Product.query.get(id)
product.comments.append(comment)
db.session.add(comment)
db.session.commit()
return jsonify(product.to_dict()), 201
```

Next I move into the api.py module where I can define a Blueprint object called api containing RESTful routes.

```
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import  JSON

db = SQLAlchemy()

class  Product(db.Model):
__tablename__ =  'products'
id  = db.Column(db.Integer, primary_key=True)
name = db.Column(db.Text)
img_src = db.Column(db.Text)
language = db.Column(db.Text)
category = db.Column(db.Text)
price = db.Column(db.Integer)
comments = db.relationship('Comment', backref="product", lazy=False)  
def  to_dict(self):
return  dict(id=self.id,
name=self.name,
img_src=self.img_src,
language=self.language,
category=self.category,
price=self.price,
comments=[comment.to_dict() for comment in  self.comments])

class  Comment(db.Model):
__tablename__ =  'comments'
id  = db.Column(db.Integer, primary_key=True)
author = db.Column(db.String(500), nullable=False)
text = db.Column(db.String(500), nullable=False)
# upvotes = db.Column(db.Integer, default=0)
product_id = db.Column(db.Integer, db.ForeignKey('products.id'))
def  to_dict(self):
return  dict(id=self.id,
author=self.author,
text=self.text,
# upvotes=self.upvotes,
product_id=self.product_id)
```

Next, we need to implement a data layer, which will require writing some data classes inside of models.py such as:

-   Product: this is the top level object that will contain product details and one or more comments
    
-   Comment: objects that belong to a product object and contain author and comment text
    

The final thing I would like to do is to bring together the Flask-Script and Flask-Migrate extension packages inside the manage.py module to enable migrations.
```
(venv) $ python manage.py db init
(venv) $ python manage.py db migrate
(venv) $ python manage.py db upgrade
```
```
(venv) $ python appserver.py
```
```
if  __name__  ==  '__main__':
from frontendapi.app import create_app
app = create_app()
app.run()
```
To start the dev server, in appserver.py I need to create an instance of the app. This enables me to fire up the Flask dev server by calling the run() method on the app instance.

Now, to run the Flask dev server

(venv) $ python appserver.py

  

### 7) AJAX Integration with REST API

[ Readings : [AJAX](https://developer.mozilla.org/en-US/docs/Web/Guide/AJAX) , [fat client](https://en.wikipedia.org/wiki/Fat_client) , [axios](https://github.com/axios/axios) , [Cross Origin Resource Sharing](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) ]

There are two major technologies that enable this fat client application to juggle the maintenance of state (data), behavior, and data driven presentation.

1.  The Vue.js framework with its excellent reactivity system shared along with the vuex flux-like library
    
2.  AJAX functionality implemented within a well designed JavaScript library called axios
    

Install axios
```
$ npm install --save axios
```
Now, in order to use axios to make AJAX requests from the client to the back-end server I will need to make a change to the Flask application to enable Cross Origin Resource Sharing (CORS).
```
(venv)$ pip install Flask-CORS  
```
```
from flask_cors import  CORS
...
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
...
```

I also need to import and instantiate the CORS extension object then register it with the Flask application object within the application.py module of the back-end application.

In src/api/index.js I need to import the axios library and for reusability I also define a variable called API_URL that is equal to the root of the API resource http://127.0.0.1:5000/api. Then I replace the body of the existing functions to use the axios methods get(...), put(...), and post(...) like so:

```
import  axios  from  'axios'
const  API_URL  =  'http://127.0.0.1:5000/api'
export  function  fetchProducts() {
return  axios.get(`${API_URL}/catalog/`)
}
export  function  fetchProduct(productId) {
return  axios.get(`${API_URL}/catalog/${productId}/`)
}
export  function  postComment(data) {
return  axios.post(`${API_URL}/catalog/${data.pid}/`, data.comment, {headers: {
'Access-Control-Allow-Origin': '*',
},})
}
```
So far on the checklist,

![](https://lh4.googleusercontent.com/dVM7UdVyeLj-73-MQDv5KJ5ToRPNMh77YyGTHXXS78n9W4bkdDxk5RC2FTougkoUhzlLK4rb8Zi4VxxZW7UMqUw1kIVzaBvrrwYPeHspXFEn-ydwK1D6w8kElzeVPIXtgcsqJjou)

