from flask import Flask
from flask_cors import CORS

def create_app(app_name='FRONTEND_API'): 
    app = Flask(__name__)
    app.config.from_object('frontendapi.config.Config')
    
    cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

    from frontendapi.api import api
    app.register_blueprint(api, url_prefix="/api")

    from frontendapi.models import db
    db.init_app(app)

    return app
