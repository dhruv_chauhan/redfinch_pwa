import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = True
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'my-secret-key'
    SQLALCHEMY_DATABASE_URI = 'postgres://znjpepoy:2SjD3Kh5BcvJhvtydWjrDremWPAxRZVh@stampy.db.elephantsql.com:5432/znjpepoy'