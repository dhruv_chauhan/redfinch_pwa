from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON

db = SQLAlchemy()

class Product(db.Model):  
    __tablename__ = 'products'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    img_src = db.Column(db.Text)
    language = db.Column(db.Text)
    category = db.Column(db.Text)
    price = db.Column(db.Integer)
    comments = db.relationship('Comment', backref="product", lazy=False)

    def to_dict(self):
        return dict(id=self.id,
                    name=self.name,
                    img_src=self.img_src,
                    language=self.language,
                    category=self.category,
                    price=self.price,
                    comments=[comment.to_dict() for comment in self.comments])

class Comment(db.Model):  
    __tablename__ = 'comments'

    id = db.Column(db.Integer, primary_key=True)
    author = db.Column(db.String(500), nullable=False)
    text = db.Column(db.String(500), nullable=False)
    # upvotes = db.Column(db.Integer, default=0)
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'))

    def to_dict(self):
        return dict(id=self.id,
                    author=self.author,
                    text=self.text,
                    # upvotes=self.upvotes,
                    product_id=self.product_id)