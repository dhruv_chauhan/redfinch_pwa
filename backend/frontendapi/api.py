from flask import Blueprint, jsonify, request
from .models import db, Product, Comment

api = Blueprint('api', __name__)

@api.route('/catalog/', methods=('GET', 'POST'))
def fetchProducts():  
    products = Product.query.all()
    return jsonify([p.to_dict() for p in products])

@api.route('/catalog/<int:id>/', methods=('GET', 'POST'))
def fetchProduct(id):  
    if request.method == 'GET':
        product = Product.query.get(id)
        return jsonify(product.to_dict())
    elif request.method == 'POST':
        data = request.get_json()
        comment = Comment(author=data['author'], text=data['text'])
        product = Product.query.get(id)
        product.comments.append(comment)
        db.session.add(comment)
        db.session.commit()
        return jsonify(product.to_dict()), 201
