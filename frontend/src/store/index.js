import Vue from 'vue'
import Vuex from 'vuex'
import {
  fetchProducts,
  fetchProduct,
  postComment
} from '@/api'

Vue.use(Vuex)

const state = {
  products: [],
  currentProduct: {},
}

const actions = {
  // asynchronous operations
  loadProducts(context) {
    return fetchProducts()
      .then((response) => {
        context.commit('setProducts', {
          products: response.data
        })
      })
  },
  loadProduct(context, {
    id
  }) {
    return fetchProduct(id)
      .then((response) => {
        context.commit('setProduct', {
          product: response.data
        })
      })
  },
  addComment(context, data) {
    return postComment(data)
  }
}

const mutations = {
  setProducts(state, payload) {
    state.products = payload.products
  },
  setProduct(state, payload) {
    state.currentProduct = payload.product
  },
}

const getters = {
  // reusable data accessors
}

const store = new Vuex.Store({
  state,
  actions,
  mutations,
  getters
})

export default store
