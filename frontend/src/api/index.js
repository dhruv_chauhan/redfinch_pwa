import axios from 'axios'

const API_URL = 'http://127.0.0.1:5000/api'

export function fetchProducts() {  
  return axios.get(`${API_URL}/catalog/`)
}

export function fetchProduct(productId) {  
  return axios.get(`${API_URL}/catalog/${productId}/`)
}

export function postComment(data) {  
  return axios.post(`${API_URL}/catalog/${data.pid}/`, data.comment, {headers: {
	  'Access-Control-Allow-Origin': '*',
	},})
}