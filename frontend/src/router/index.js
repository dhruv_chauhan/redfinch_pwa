import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import Router from 'vue-router'
import store from '@/store'
import Home from '@/components/Home'
import Catalog from '@/components/Catalog'
import Products from '@/components/Products'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


Vue.use(BootstrapVue)
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Catalog',
      component: Catalog
    },{
      path: '/catalog',
      name: 'Catalog',
      component: Catalog
    },{
      path: '/catalog/:id',
      name: 'Products',
      component: Products
    },
  ]
})
